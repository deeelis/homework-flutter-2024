import '../dto/articles_api.dart';

abstract interface class ArticlesDao {
  Future<ArticlesApi?> getData(
    int index,
    String firstLink,
    String secondLink,
  );
}
