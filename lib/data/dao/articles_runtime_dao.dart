import 'dart:convert';

import 'package:first_homework/data/dao/articles_dao.dart';
import 'package:first_homework/data/dto/articles_api.dart';
import 'package:http/http.dart' as http;

class ArticlesRuntimeDao implements ArticlesDao {
  ArticlesRuntimeDao();

  @override
  Future<ArticlesApi?> getData(
    int index,
    String firstLink,
    String secondLink,
  ) async {
    final userDataUrl = Uri.parse(index == 0 ? firstLink : secondLink);
    final userDataResponse = await http.get(userDataUrl);
    if (userDataResponse.statusCode != 200) {
      return null;
    }
    final articles = ArticlesApi.fromJson(jsonDecode(userDataResponse.body));
    return articles;
  }
}
