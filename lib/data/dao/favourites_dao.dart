import '../dto/article_dto.dart';

abstract interface class FavouritesDao {
  Future<ArticleDto> addFavorite(ArticleDto article);
  Future<void> deleteFavorite(ArticleDto article);
  Future<List<ArticleDto>> getAll();
}
