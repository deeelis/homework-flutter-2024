import 'dart:developer';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../dto/article_dto.dart';
import 'favourites_dao.dart';

class FavouritesRuntimeDao implements FavouritesDao {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, 'database.db'),
      onCreate: (database, version) async {
        await database.execute(
          """CREATE TABLE Favourites(
            uuid TEXT PRIMARY KEY ,
            author TEXT NOT NULL ,
            title TEXT NOT NULL ,
            description TEXT NOT NULL ,
            url TEXT NOT NULL ,
            urlToImage TEXT NOT NULL ,
            publishedAt TEXT NOT NULL 
            )
            """,
        );
      },
      version: 1,
    );
  }

  @override
  Future<ArticleDto> addFavorite(ArticleDto article) async {
    final Database db = await initializeDB();
    final _ = await db.insert(
      'Favourites',
      article.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
      nullColumnHack: null,
    );

    return article;
  }

  @override
  Future<void> deleteFavorite(ArticleDto article) async {
    final Database db = await initializeDB();
    try {
      await db.delete(
        "Favourites",
        where: """
      author = ? and 
      title = ? and 
      description = ? and 
      url = ? and 
      urlToImage = ? and 
      publishedAt = ?  
      """,
        whereArgs: [
          article.author,
          article.title,
          article.description,
          article.url,
          article.urlToImage,
          article.publishedAt,
        ],
      );
    } catch (err) {
      log(err.toString());
    }
  }

  @override
  Future<List<ArticleDto>> getAll() async {
    final Database db = await initializeDB();
    final List<Map<String, Object?>> queryResult = await db.query('Favourites');
    return queryResult.map((e) {
      ArticleDto article = ArticleDto.fromJson(e);
      return article;
    }).toList();
  }
}
