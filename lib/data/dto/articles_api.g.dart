// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'articles_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArticlesApi _$ArticlesApiFromJson(Map<String, dynamic> json) => ArticlesApi(
      json['status'] as String? ?? '',
      json['totalResults'] as int? ?? 0,
      (json['articles'] as List<dynamic>?)
              ?.map((e) => ArticleApi.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
    );

Map<String, dynamic> _$ArticlesApiToJson(ArticlesApi instance) =>
    <String, dynamic>{
      'status': instance.status,
      'totalResults': instance.totalResults,
      'articles': instance.articles,
    };
