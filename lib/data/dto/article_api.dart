import 'package:first_homework/data/dto/source_api.dart';
import 'package:json_annotation/json_annotation.dart';

part 'article_api.g.dart';

@JsonSerializable()
class ArticleApi {
  final SourceApi source;
  @JsonKey(defaultValue: '')
  final String author;
  @JsonKey(defaultValue: '')
  final String title;
  @JsonKey(defaultValue: '')
  final String description;
  @JsonKey(defaultValue: '')
  final String url;
  @JsonKey(defaultValue: '')
  final String urlToImage;
  @JsonKey(defaultValue: '')
  final String publishedAt;
  @JsonKey(defaultValue: '')
  final String content;

  const ArticleApi(
    this.author,
    this.title,
    this.description,
    this.url,
    this.urlToImage,
    this.publishedAt,
    this.content, {
    this.source = const SourceApi('', ''),
  });

  factory ArticleApi.fromJson(Map<String, dynamic> json) =>
      _$ArticleApiFromJson(json);

  Map<String, dynamic> toJson() => _$ArticleApiToJson(this);
}
