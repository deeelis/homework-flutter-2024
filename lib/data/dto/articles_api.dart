import 'package:json_annotation/json_annotation.dart';

import 'article_api.dart';

part 'articles_api.g.dart';

@JsonSerializable()
class ArticlesApi {
  @JsonKey(defaultValue: '')
  final String status;
  @JsonKey(defaultValue: 0)
  final int totalResults;
  @JsonKey(defaultValue: [])
  final List<ArticleApi> articles;

  const ArticlesApi(this.status, this.totalResults, this.articles);

  factory ArticlesApi.fromJson(Map<String, dynamic> json) =>
      _$ArticlesApiFromJson(json);

  Map<String, dynamic> toJson() => _$ArticlesApiToJson(this);
}
