import 'package:json_annotation/json_annotation.dart';

part 'source_api.g.dart';

@JsonSerializable()
class SourceApi {
  @JsonKey(defaultValue: '')
  final String id;
  @JsonKey(defaultValue: '')
  final String name;

  const SourceApi(this.id, this.name);

  factory SourceApi.fromJson(Map<String, dynamic> json) =>
      _$SourceApiFromJson(json);

  Map<String, dynamic> toJson() => _$SourceApiToJson(this);
}
