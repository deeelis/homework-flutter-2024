// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArticleApi _$ArticleApiFromJson(Map<String, dynamic> json) => ArticleApi(
      json['author'] as String? ?? '',
      json['title'] as String? ?? '',
      json['description'] as String? ?? '',
      json['url'] as String? ?? '',
      json['urlToImage'] as String? ?? '',
      json['publishedAt'] as String? ?? '',
      json['content'] as String? ?? '',
      source: json['source'] == null
          ? const SourceApi('', '')
          : SourceApi.fromJson(json['source'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ArticleApiToJson(ArticleApi instance) =>
    <String, dynamic>{
      'source': instance.source,
      'author': instance.author,
      'title': instance.title,
      'description': instance.description,
      'url': instance.url,
      'urlToImage': instance.urlToImage,
      'publishedAt': instance.publishedAt,
      'content': instance.content,
    };
