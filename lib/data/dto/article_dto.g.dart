// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArticleDto _$ArticleDtoFromJson(Map<String, dynamic> json) => ArticleDto(
      json['uuid'] as String? ?? '',
      json['author'] as String? ?? '',
      json['title'] as String? ?? '',
      json['description'] as String? ?? '',
      json['url'] as String? ?? '',
      json['urlToImage'] as String? ?? '',
      json['publishedAt'] as String? ?? '',
    );

Map<String, dynamic> _$ArticleDtoToJson(ArticleDto instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'author': instance.author,
      'title': instance.title,
      'description': instance.description,
      'url': instance.url,
      'urlToImage': instance.urlToImage,
      'publishedAt': instance.publishedAt,
    };
