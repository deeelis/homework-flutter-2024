// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'source_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SourceApi _$SourceApiFromJson(Map<String, dynamic> json) => SourceApi(
      json['id'] as String? ?? '',
      json['name'] as String? ?? '',
    );

Map<String, dynamic> _$SourceApiToJson(SourceApi instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
