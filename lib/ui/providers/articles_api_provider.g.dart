// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'articles_api_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$articlesApiHash() => r'085a4d0d66250eb0ade413fcb2a3e578b23cd12c';

/// See also [ArticlesApi].
@ProviderFor(ArticlesApi)
final articlesApiProvider =
    AutoDisposeAsyncNotifierProvider<ArticlesApi, Articles>.internal(
  ArticlesApi.new,
  name: r'articlesApiProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$articlesApiHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ArticlesApi = AutoDisposeAsyncNotifier<Articles>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
