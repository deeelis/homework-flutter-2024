import 'package:first_homework/core/providers.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../domain/models/article.dart';
import '../../domain/models/articles.dart';
part 'favourites_provider.g.dart';

@riverpod
class Favourites extends _$Favourites {
  @override
  Future<Articles> build() async {
    Articles articles = await ref.read(favouritesRepositoryProvider).getAll();
    return articles;
  }

  bool isFavourite(Article article) {
    bool isFavorite = false;
    if (state.value != null) {
      for (int i = 0; i < state.value!.articles.length; i++) {
        if (state.value!.articles[i] == article) {
          isFavorite = true;
          break;
        }
      }
    }
    return isFavorite;
  }

  Future<void> addFavourite(Article article) async {
    bool isFavouriteArticle = isFavourite(article);
    if (!isFavouriteArticle) {
      Articles articles =
          await ref.read(favouritesRepositoryProvider).addFavorite(
                ref.read(articleMapperProvider).mapArticleToArticleDto(article),
              );
      state = AsyncValue.data(articles);
    }
  }

  Future<void> deleteFavourite(Article article) async {
    Articles articles =
        await ref.read(favouritesRepositoryProvider).deleteFavorite(
              ref.read(articleMapperProvider).mapArticleToArticleDto(article),
            );
    state = AsyncValue.data(articles);
  }
}
