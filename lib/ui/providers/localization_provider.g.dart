// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'localization_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$localizationHash() => r'926f5f0e61a1409b8cb0f9beac1faf467f69678f';

/// See also [Localization].
@ProviderFor(Localization)
final localizationProvider =
    AutoDisposeNotifierProvider<Localization, String>.internal(
  Localization.new,
  name: r'localizationProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$localizationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Localization = AutoDisposeNotifier<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
