import 'package:first_homework/core/providers.dart';
import 'package:first_homework/domain/models/article.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../domain/models/articles.dart';
part 'articles_api_provider.g.dart';

@riverpod
class ArticlesApi extends _$ArticlesApi {
  @override
  Future<Articles> build() async {
    Articles articles = await ref.read(articleRepositoryProvider).getData(
          1,
          "https://newsapi.org/v2/top-headlines?language=en&apiKey=e4f1559121714e5594c4ebb9d91e2a4a",
          "https://newsapi.org/v2/everything?q=cats&language=en&apiKey=e4f1559121714e5594c4ebb9d91e2a4a",
        );
    return articles;
  }

  Future<Articles> updateArticles(
    int index,
    String firstLink,
    String secondLink,
    Map<String, dynamic> filter,
  ) async {
    Articles articles = await ref
        .read(articleRepositoryProvider)
        .getData(index, firstLink, secondLink);
    List<Article> filterArticles = [];
    for (int i = 0; i < articles.articles.length; ++i) {
      if (articles.articles[i].urlToImage != "" || !filter["isImage"]) {
        if (!filter["isData"] ||
            (DateTime.parse(filter["from"]).isBefore(
                    DateTime.parse(articles.articles[i].publishedAt),) &&
                DateTime.parse(filter["to"]).isAfter(
                    DateTime.parse(articles.articles[i].publishedAt),))) {
          if (!filter.containsKey("text") ||
              articles.articles[i].title.contains(filter["text"])) {
            filterArticles.add(articles.articles[i]);
          }
        }
      }
    }
    state = AsyncData(Articles(articles.totalResults, filterArticles));
    return Articles(articles.totalResults, filterArticles);
  }
}
