// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favourites_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$favouritesHash() => r'7fa336e69cd547a210cf7f1a3029ab079dba7b19';

/// See also [Favourites].
@ProviderFor(Favourites)
final favouritesProvider =
    AutoDisposeAsyncNotifierProvider<Favourites, Articles>.internal(
  Favourites.new,
  name: r'favouritesProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$favouritesHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Favourites = AutoDisposeAsyncNotifier<Articles>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
