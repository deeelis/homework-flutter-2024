import 'package:first_homework/ui/providers/favourites_provider.dart';
import 'package:first_homework/ui/providers/theme_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import '../../domain/models/article.dart';
import '../../domain/models/articles.dart';

class DetailsPage extends ConsumerWidget {
  final Article article;

  const DetailsPage({super.key, required this.article});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Articles _ = ref.watch(favouritesProvider).value ?? const Articles(0, []);
    bool isFavourite =
        ref.watch(favouritesProvider.notifier).isFavourite(article);
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(AppLocalizations.of(context)!.details)),
        backgroundColor:
            ref.watch(themeProvider) ? Colors.blueGrey : Colors.pink[100],
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(
              ref.watch(themeProvider) ? Icons.nightlight : Icons.sunny,
            ),
            onPressed: () {
              ref.watch(themeProvider.notifier).changeTheme();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(
                  4,
                ),
                child: Image.network(
                  article.urlToImage,
                  errorBuilder: (
                    BuildContext context,
                    Object exception,
                    StackTrace? stackTrace,
                  ) {
                    return const SizedBox();
                  },
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            const SizedBox(height: 16.0),
            Hero(
              tag: "${article.url}title",
              child: Text(
                article.title,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  color: ref.watch(themeProvider) ? Colors.white : Colors.black,
                  decoration: TextDecoration.none,
                ),
              ),
            ),
            const SizedBox(height: 8.0),
            Hero(
              tag: "${article.url}description",
              child: Text(
                article.description,
                style: TextStyle(
                  fontSize: 16.0,
                  color: ref.watch(themeProvider) ? Colors.white : Colors.black,
                  decoration: TextDecoration.none,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
            // Text(
            //   article.description,
            //   style: const TextStyle(fontSize: 16.0),
            // ),
            const SizedBox(height: 8.0),
            Center(
              child: TextButton(
                onPressed: () {
                  launchUrlString(article.url);
                },
                child: Text(
                  AppLocalizations.of(context)!.more,
                  style: const TextStyle(fontSize: 16.0),
                ),
              ),
            ),
            const SizedBox(height: 16.0),
          ],
        ),
      ),
      floatingActionButton: Hero(
        tag: article.url,
        child: SpeedDial(
          icon: isFavourite ? Icons.star : Icons.star_border,
          backgroundColor:
              ref.watch(themeProvider) ? Colors.blueGrey : Colors.pink[100],
          activeIcon: Icons.close,
          curve: Curves.bounceInOut,
          elevation: 8.0,
          shape: const CircleBorder(),
          children: [
            SpeedDialChild(
              child: Icon(isFavourite ? Icons.star_border : Icons.star),
              shape: const CircleBorder(),
              elevation: 8.0,
              label: isFavourite
                  ? AppLocalizations.of(context)!.delete_message
                  : AppLocalizations.of(context)!.add_message,
              backgroundColor:
                  ref.watch(themeProvider) ? Colors.blueGrey : Colors.pink[100],
              onTap: () {
                isFavourite
                    ? ref
                        .watch(favouritesProvider.notifier)
                        .deleteFavourite(article)
                    : ref
                        .watch(favouritesProvider.notifier)
                        .addFavourite(article);
                isFavourite = !isFavourite;
              },
            ),
          ],
        ),
      ),
    );
  }
}
