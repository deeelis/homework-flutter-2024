import 'package:first_homework/domain/models/articles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../widgets/list/news_list.dart';
import '../providers/favourites_provider.dart';
import '../providers/theme_provider.dart';

class FavoritesPage extends ConsumerWidget {
  const FavoritesPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Future<Articles> favourites = ref.watch(favouritesProvider.future);
    return Scaffold(
      appBar: AppBar(
        title:
            Center(child: Text(AppLocalizations.of(context)!.favourites_title)),
        backgroundColor:
            ref.watch(themeProvider) ? Colors.blueGrey : Colors.pink[100],
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(
              ref.watch(themeProvider) ? Icons.nightlight : Icons.sunny,
            ),
            onPressed: () {
              ref.watch(themeProvider.notifier).changeTheme();
            },
          ),
        ],
      ),
      body: FutureBuilder(
        future: favourites,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return NewsList(articles: snapshot.data);
          } else if (snapshot.hasError) {
            return Center(
              child: Text('${snapshot.error}'),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
