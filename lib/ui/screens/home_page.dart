import 'package:first_homework/domain/models/articles.dart';
import 'package:first_homework/ui/widgets/list/list_item.dart';
import 'package:first_homework/ui/providers/articles_api_provider.dart';
import 'package:first_homework/ui/providers/localization_provider.dart';
import 'package:first_homework/ui/providers/theme_provider.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:animations/animations.dart';

import '../widgets/filter.dart';
import 'favourites_page.dart';

class MyHomePage extends ConsumerStatefulWidget {
  const MyHomePage({
    super.key,
  });

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends ConsumerState<MyHomePage>
    with TickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _tabController.index = 0;
  }

  final _tabs = <Widget>[
    const HomeContent(index: 0, key: ValueKey(0)),
    const HomeContent(index: 1, key: ValueKey(1)),
  ];

  @override
  Widget build(BuildContext context) {
    final Future<Articles> _ = ref.watch(articlesApiProvider.future);
    return Scaffold(
      appBar: AppBar(
        backgroundColor:
            ref.watch(themeProvider) ? Colors.blueGrey : Colors.pink[100],
        title: Text(
          _tabController.index == 0
              ? AppLocalizations.of(context)!.first_title
              : AppLocalizations.of(context)!.second_title,
        ),
        actions: [
          AnimatedSwitcher(
            duration: const Duration(milliseconds: 500),
            child: TextButton.icon(
              key: ValueKey<String>(AppLocalizations.of(context)!.lang),
              onPressed: () {
                ref.watch(localizationProvider.notifier).changeLocale();
              },
              icon: const Icon(Icons.language),
              label: Text(AppLocalizations.of(context)!.lang),
            ),
            transitionBuilder: (Widget child, Animation<double> animation) {
              return SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(-0.5, 0.0),
                  end: Offset.zero,
                ).animate(animation),
                child: FadeTransition(
                  opacity: animation,
                  child: child,
                ),
              );
            },
          ),
          GestureDetector(
            key: const Key("theme"),
            onTap: ref.watch(themeProvider.notifier).changeTheme,
            child: AnimatedContainer(
              duration: const Duration(milliseconds: 500),
              curve: Curves.easeInOut,
              child: AnimatedCrossFade(
                duration: const Duration(milliseconds: 500),
                firstChild: const Icon(
                  Icons.sunny,
                ),
                secondChild: const Icon(
                  Icons.nightlight,
                ),
                crossFadeState: ref.watch(themeProvider)
                    ? CrossFadeState.showSecond
                    : CrossFadeState.showFirst,
              ),
            ),
          ),
          IconButton(
            key: const Key("fav"),
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const FavoritesPage(
                  key: Key("favPage"),
                ),
              ),
            ),
            icon: const Icon(
              Icons.star,
            ),
          ),
        ],
      ),
      body: TabBarView(
        controller: _tabController,
        physics: const BouncingScrollPhysics(),
        dragStartBehavior: DragStartBehavior.down,
        children: _tabs,
      ),
      bottomNavigationBar: BottomAppBar(
        height: 70,
        padding: const EdgeInsets.symmetric(),
        color: ref.watch(themeProvider) ? Colors.blueGrey : Colors.pink[100],
        child: TabBar(
          indicator: const BoxDecoration(),
          labelStyle: const TextStyle(fontWeight: FontWeight.bold),
          unselectedLabelStyle: const TextStyle(fontStyle: FontStyle.italic),
          controller: _tabController,
          tabs: [
            Tab(
              icon: const Icon(Icons.trending_up),
              text: AppLocalizations.of(context)!.first_label,
            ),
            Tab(
              icon: const Icon(Icons.tag_faces),
              text: AppLocalizations.of(context)!.second_label,
            ),
          ],
        ),
      ),
    );
  }
}

class HomeContent extends ConsumerStatefulWidget {
  const HomeContent({super.key, required this.index});

  final int index;

  @override
  HomeContentState createState() => HomeContentState();
}

class HomeContentState extends ConsumerState<HomeContent> {
  late Future<Articles?> articles;
  late TextEditingController _searchController;
  late Map<String, dynamic> filter;

  @override
  void initState() {
    //articles = getData(widget.index, AppLocalizations.of(context)!.first_link, AppLocalizations.of(context)!.second_link);
    super.initState();
    filter = {
      "isData": false,
      "from": DateFormat('yyyy-MM-dd').format(DateTime(2000)),
      "to": DateFormat('yyyy-MM-dd').format(DateTime.now()),
      "isImage": false,
    };
    _searchController = TextEditingController();
  }

  void _submit(String text) {
    String text = _searchController.text;
    if (text != "") {
      setState(() {
        filter["text"] = text;
        Future<Articles> _ =
            ref.watch(articlesApiProvider.notifier).updateArticles(
                  widget.index,
                  AppLocalizations.of(context)!.first_link,
                  AppLocalizations.of(context)!.second_link,
                  filter,
                );
      });
    } else {
      setState(() {
        filter.remove("text");
        Future<Articles> _ =
            ref.watch(articlesApiProvider.notifier).updateArticles(
                  widget.index,
                  AppLocalizations.of(context)!.first_link,
                  AppLocalizations.of(context)!.second_link,
                  filter,
                );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final Future<Articles> articles = ref.watch(articlesApiProvider.future);
    final SliverAppBar sliverAppBar = SliverAppBar(
      floating: true,
      snap: true,
      title: SizedBox(
        width: double.infinity,
        height: 40,
        child: SearchBar(
          onSubmitted: _submit,
          controller: _searchController,
          hintText: AppLocalizations.of(context)!.search,
          leading: IconButton(
            onPressed: () => showModal(
              configuration: const FadeScaleTransitionConfiguration(
                transitionDuration: Duration(milliseconds: 500),
                reverseTransitionDuration: Duration(milliseconds: 300),
              ),
              context: context,
              builder: (BuildContext context) => Filter(
                filter: filter,
                index: widget.index,
              ),
            ),
            icon: const Icon(Icons.filter_list),
          ),
          elevation: MaterialStateProperty.all(0),
          trailing: [
            IconButton(
              icon: const Icon(Icons.clear),
              onPressed: () => {
                _searchController.clear(),
                _submit(_searchController.text),
              },
            ),
            IconButton(
              onPressed: () => _submit(_searchController.text),
              icon: const Icon(Icons.search),
            ),
          ],
          backgroundColor: MaterialStateProperty.all(
              ref.watch(themeProvider) ? Colors.blueGrey : Colors.pink[100],),
        ),
      ),
    );
    return Scaffold(
      body: FutureBuilder(
        future: articles,
        builder: (context, snapshot) {
          Widget newsListSliver;
          if (snapshot.connectionState != ConnectionState.done) {
            newsListSliver = const SliverToBoxAdapter(
              child: SizedBox(
                height: 300,
                width: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            );
          } else if (snapshot.hasData) {
            newsListSliver = SliverList.separated(
              itemCount: snapshot.data!.articles.length,
              itemBuilder: (context, index) =>
                  ListItem(article: snapshot.data!.articles[index]),
              separatorBuilder: (context, index) => const Divider(
                indent: 20,
                endIndent: 20,
              ),
            );
          } else if (snapshot.hasError) {
            newsListSliver = SliverToBoxAdapter(
              child: Center(
                child: Text('${snapshot.error}'),
              ),
            );
          } else {
            newsListSliver = const SliverToBoxAdapter(
              child: SizedBox(
                height: 300,
                width: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            );
          }
          return CustomScrollView(
            slivers: [
              sliverAppBar,
              newsListSliver,
            ],
          );
        },
      ),
    );
  }

  @override
  void didChangeDependencies() {
    articles = ref.watch(articlesApiProvider.notifier).updateArticles(
          widget.index,
          AppLocalizations.of(context)!.first_link,
          AppLocalizations.of(context)!.second_link,
          filter,
        );
    super.didChangeDependencies();
  }
}
