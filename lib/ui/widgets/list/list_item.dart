import 'package:first_homework/ui/providers/favourites_provider.dart';
import 'package:first_homework/ui/providers/theme_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../domain/models/article.dart';
import '../../../domain/models/articles.dart';
import '../../screens/details_page.dart';

class ListItem extends ConsumerStatefulWidget {
  final Article article;
  ListItem({super.key, required this.article}) {
    super.key;
  }

  @override
  ListItemState createState() => ListItemState();
}

class ListItemState extends ConsumerState<ListItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Articles _ = ref.watch(favouritesProvider).value ?? const Articles(0, []);
    bool isFavourite =
        ref.watch(favouritesProvider.notifier).isFavourite(widget.article);
    return Padding(
      padding: const EdgeInsets.only(
        left: 20,
        right: 14,
      ),
      child: Dismissible(
        key: UniqueKey(),
        direction: DismissDirection.horizontal,
        confirmDismiss: (DismissDirection direction) async {
          if (direction == DismissDirection.endToStart) {
            ref
                .watch(favouritesProvider.notifier)
                .deleteFavourite(widget.article);
          } else {
            ref.watch(favouritesProvider.notifier).addFavourite(widget.article);
          }
          return false;
        },
        background: ColoredBox(
          color: ref.watch(themeProvider)
              ? Colors.deepOrangeAccent[100] ?? Colors.deepOrangeAccent
              : Colors.yellow[300] ?? Colors.orange,
          child: const Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Icon(Icons.star, color: Colors.white),
            ),
          ),
        ),
        secondaryBackground: ColoredBox(
          color: ref.watch(themeProvider)
              ? Colors.blueGrey
              : Colors.pink[100] ?? Colors.pink,
          child: const Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Icon(Icons.delete, color: Colors.white),
            ),
          ),
        ),
        child: Card(
          clipBehavior: Clip.hardEdge,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetailsPage(article: widget.article),
                ),
              );
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 16.0),
                        Hero(
                          tag: "${widget.article.url}title",
                          child: Text(
                            widget.article.title,
                            style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              color: ref.watch(themeProvider)
                                  ? Colors.white
                                  : Colors.black,
                              decoration: TextDecoration.none,
                            ),
                          ),
                        ),
                        const SizedBox(height: 8.0),
                        Hero(
                          tag: "${widget.article.url}description",
                          child: Text(
                            widget.article.description,
                            style: TextStyle(
                              fontSize: 16.0,
                              color: ref.watch(themeProvider)
                                  ? Colors.white
                                  : Colors.black,
                              decoration: TextDecoration.none,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                        // Text(
                        //   widget.article.description,
                        //   style: const TextStyle(fontSize: 16.0),
                        // ),
                        const SizedBox(height: 16.0),
                      ],
                    ),
                  ),
                  Hero(
                    tag: widget.article.url,
                    child: Icon(
                      isFavourite ? Icons.star : Icons.star_border,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
