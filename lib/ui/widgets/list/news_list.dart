import 'package:flutter/material.dart';

import '../../../domain/models/articles.dart';
import 'list_item.dart';

class NewsList extends StatelessWidget {
  late final Articles news;

  NewsList({super.key, required articles}) {
    news = articles;
    super.key;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 0),
      child: ListView.separated(
        itemCount: news.articles.length,
        itemBuilder: (context, index) =>
            ListItem(article: news.articles[index]),
        separatorBuilder: (context, index) => const Divider(
          indent: 20,
          endIndent: 20,
        ),
      ),
    );
  }
}
