import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

import '../../domain/models/articles.dart';
import '../providers/articles_api_provider.dart';

class Filter extends ConsumerStatefulWidget {
  const Filter({super.key, required this.filter, required this.index});
  final Map<String, dynamic> filter;
  final int index;

  @override
  FilterState createState() => FilterState();
}

class FilterState extends ConsumerState<Filter> {
  late TextEditingController _fromController;
  late TextEditingController _toController;
  late bool _isImage;
  late bool _isData;

  @override
  void initState() {
    _fromController = TextEditingController();
    _toController = TextEditingController();
    super.initState();
    _fromController.text = widget.filter["from"];
    _toController.text = widget.filter["to"];
    _isImage = widget.filter["isImage"];
    _isData = widget.filter["isData"];
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Text(AppLocalizations.of(context)!.date),
                const Spacer(),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: Checkbox(
                    value: _isData,
                    onChanged: (value) {
                      setState(() {
                        _isData = value!;
                      });
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: [
                const Spacer(),
                Text(AppLocalizations.of(context)!.from),
                const Spacer(),
                SizedBox(
                  width: 100,
                  child: TextField(
                    enabled: _isData ? true : false,
                    controller: _fromController,
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2000),
                        lastDate: DateTime.now(),
                      );

                      if (pickedDate != null) {
                        String formattedDate =
                        DateFormat('yyyy-MM-dd').format(pickedDate);
                        setState(() {
                          _fromController.text = formattedDate;
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: [
                const Spacer(),
                Text(AppLocalizations.of(context)!.to),
                const Spacer(),
                SizedBox(
                  width: 100,
                  child: TextField(
                    enabled: _isData ? true : false,
                    controller: _toController,
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2000),
                        lastDate: DateTime.now(),
                      );

                      if (pickedDate != null) {
                        String formattedDate =
                        DateFormat('yyyy-MM-dd').format(pickedDate);
                        setState(() {
                          _toController.text = formattedDate;
                        });
                      } else {}
                    },
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            Row(
              children: [
                Text(AppLocalizations.of(context)!.is_image),
                const Spacer(),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: Checkbox(
                    value: _isImage,
                    onChanged: (value) {
                      setState(() {
                        _isImage = value!;
                      });
                    },
                  ),
                ),
              ],
            ),
            TextButton(
              onPressed: () {
                setState(() {
                  widget.filter["isData"] = _isData;
                  widget.filter["from"] = _fromController.text;
                  widget.filter["to"] = _toController.text;
                  widget.filter["isImage"] = _isImage;
                  Future<Articles> _ =
                  ref.watch(articlesApiProvider.notifier).updateArticles(
                    widget.index,
                    AppLocalizations.of(context)!.first_link,
                    AppLocalizations.of(context)!.second_link,
                    widget.filter,
                  );
                });
                Navigator.pop(context);
              },
              child: Text(AppLocalizations.of(context)!.save),
            ),
          ],
        ),
      ),
    );
  }
}
