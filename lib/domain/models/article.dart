import 'package:json_annotation/json_annotation.dart';

part 'article.g.dart';

@JsonSerializable()
class Article {
  @JsonKey(defaultValue: '')
  final String author;
  @JsonKey(defaultValue: '')
  final String title;
  @JsonKey(defaultValue: '')
  final String description;
  @JsonKey(defaultValue: '')
  final String url;
  @JsonKey(defaultValue: '')
  final String urlToImage;
  @JsonKey(defaultValue: '')
  final String publishedAt;

  Article(
    this.author,
    this.title,
    this.description,
    this.url,
    this.urlToImage,
    this.publishedAt,
  );

  factory Article.fromJson(Map<String, dynamic> json) =>
      _$ArticleFromJson(json);

  Map<String, dynamic> toJson() => _$ArticleToJson(this);

  @override
  bool operator ==(Object other) {
    if (other is! Article) return false;
    if (author != other.author) return false;
    if (title != other.title) return false;
    if (description != other.description) return false;
    if (url != other.url) return false;
    if (urlToImage != other.urlToImage) return false;
    if (publishedAt != other.publishedAt) return false;
    return true;
  }

  @override
  int get hashCode => title.hashCode;
}
