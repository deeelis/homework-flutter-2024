import 'package:json_annotation/json_annotation.dart';

import 'article.dart';

part 'articles.g.dart';

@JsonSerializable()
class Articles {
  @JsonKey(defaultValue: 0)
  final int totalResults;
  @JsonKey(defaultValue: [])
  final List<Article> articles;

  const Articles(this.totalResults, this.articles);

  factory Articles.fromJson(Map<String, dynamic> json) =>
      _$ArticlesFromJson(json);

  Map<String, dynamic> toJson() => _$ArticlesToJson(this);
}
