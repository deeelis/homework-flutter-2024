import '../models/articles.dart';

abstract interface class ArticlesRepository {
  Future<Articles> getData(
    int index,
    String firstLink,
    String secondLink,
  );
}
