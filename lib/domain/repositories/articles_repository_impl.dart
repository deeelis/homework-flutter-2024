import 'package:first_homework/data/dao/articles_dao.dart';
import 'package:first_homework/data/dto/articles_api.dart';
import 'package:first_homework/domain/mappers/article_mapper.dart';

import '../models/article.dart';
import '../models/articles.dart';
import 'articles_repository.dart';

class ArticlesRepositoryImpl implements ArticlesRepository {
  final ArticlesDao articlesDao;
  final ArticleMapper mapper;

  ArticlesRepositoryImpl(this.articlesDao, this.mapper);

  @override
  Future<Articles> getData(
    int index,
    String firstLink,
    String secondLink,
  ) async {
    ArticlesApi? articlesApi = await articlesDao.getData(
      index,
      firstLink,
      secondLink,
    );
    if (articlesApi != null) {
      Articles articles = Articles(articlesApi.totalResults, []);
      for (int i = 0; i < articlesApi.articles.length; i++) {
        Article article = mapper.mapArticleApiToArticle(
          articlesApi.articles[i],
        );
        articles.articles.add(article);
      }

      return Future.value(articles);
    }
    return Future.value(const Articles(0, []));
  }
}
