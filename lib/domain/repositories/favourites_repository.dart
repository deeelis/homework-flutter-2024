import '../../data/dto/article_dto.dart';
import '../models/articles.dart';

abstract interface class FavouritesRepository {
  Future<Articles> addFavorite(ArticleDto article);
  Future<Articles> deleteFavorite(ArticleDto article);
  Future<Articles> getAll();
}
