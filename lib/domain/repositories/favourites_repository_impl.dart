import 'package:first_homework/data/dao/favourites_dao.dart';
import 'package:first_homework/data/dto/article_dto.dart';

import '../models/article.dart';
import '../models/articles.dart';
import '../mappers/article_mapper.dart';
import 'favourites_repository.dart';

class FavouritesRepositoryImpl implements FavouritesRepository {
  final FavouritesDao favoritesDao;
  final ArticleMapper mapper;

  FavouritesRepositoryImpl(this.favoritesDao, this.mapper);

  @override
  Future<Articles> addFavorite(ArticleDto article) async {
    favoritesDao.addFavorite(article);
    List<ArticleDto> articlesDto = await favoritesDao.getAll();
    Articles articles = Articles(articlesDto.length, []);
    for (int i = 0; i < articlesDto.length; i++) {
      Article article = mapper.mapArticleDtoToArticle(articlesDto[i]);
      articles.articles.add(article);
    }
    return articles;
  }

  @override
  Future<Articles> deleteFavorite(ArticleDto article) async {
    favoritesDao.deleteFavorite(article);
    List<ArticleDto> articlesDto = await favoritesDao.getAll();
    Articles articles = Articles(articlesDto.length, []);
    for (int i = 0; i < articlesDto.length; i++) {
      Article article = mapper.mapArticleDtoToArticle(articlesDto[i]);
      articles.articles.add(article);
    }
    return articles;
  }

  @override
  Future<Articles> getAll() async {
    List<ArticleDto> articlesDto = await favoritesDao.getAll();
    Articles articles = Articles(articlesDto.length, []);
    for (int i = 0; i < articlesDto.length; i++) {
      Article article = mapper.mapArticleDtoToArticle(articlesDto[i]);
      articles.articles.add(article);
    }
    return articles;
  }
}
