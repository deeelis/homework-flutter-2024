import 'package:uuid/uuid.dart';

import '../models/article.dart';
import '../../data/dto/article_api.dart';
import '../../data/dto/article_dto.dart';

class ArticleMapper {
  final Uuid _uuid;

  ArticleMapper(this._uuid);

  ArticleDto mapArticleToArticleDto(Article article) => ArticleDto(
        _uuid.v1(),
        article.author,
        article.title,
        article.description,
        article.url,
        article.urlToImage,
        article.publishedAt,
      );

  Article mapArticleDtoToArticle(ArticleDto article) => Article(
        article.author,
        article.title,
        article.description,
        article.url,
        article.urlToImage,
        article.publishedAt,
      );

  Article mapArticleApiToArticle(ArticleApi article) => Article(
        article.author,
        article.title,
        article.description,
        article.url,
        article.urlToImage,
        article.publishedAt,
      );
}
