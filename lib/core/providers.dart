import 'package:first_homework/data/dao/articles_dao.dart';
import 'package:first_homework/data/dao/articles_runtime_dao.dart';
import 'package:first_homework/data/dao/favourites_dao.dart';
import 'package:first_homework/data/dao/favourites_runtime_dao.dart';
import 'package:first_homework/domain/mappers/article_mapper.dart';
import 'package:first_homework/domain/repositories/articles_repository_impl.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../domain/repositories/articles_repository.dart';
import '../domain/repositories/favourites_repository.dart';
import '../domain/repositories/favourites_repository_impl.dart';

final uuidProvider = Provider((ref) => const Uuid());

final articlesDaoProvider = Provider<ArticlesDao>(
  (ref) => ArticlesRuntimeDao(),
);

final articleMapperProvider = Provider<ArticleMapper>(
  (ref) => ArticleMapper(ref.read(uuidProvider)),
);

final articleRepositoryProvider = Provider<ArticlesRepository>(
  (ref) => ArticlesRepositoryImpl(
    ref.read(articlesDaoProvider),
    ref.read(articleMapperProvider),
  ),
);

final favouritesDaoProvider = Provider<FavouritesDao>(
  (ref) => FavouritesRuntimeDao(),
);

final favouritesRepositoryProvider = Provider<FavouritesRepository>(
  (ref) => FavouritesRepositoryImpl(
    ref.read(favouritesDaoProvider),
    ref.read(articleMapperProvider),
  ),
);
