# News App

Реализовано мобильное приложение для просмотра новостных статей с тремя экранами. 

## Основной экран

На этом экране отображается список новостей, полученных из удаленного [API](https://newsapi.org/). Пользователи могут прокручивать список, чтобы увидеть доступные новостные позиции. Есть два потока новостей: топ новости на выбранном языке и новости о котиках.

## Экран детальной информации

Создан вторичный экран, доступный из основного по клику на новость, который может отображать подробности по выбранной новостной статье.

## Экран избранных новостей

Создан экран, доступный из основного по клику на иконку звездочки в правом верхнем углу, который может отображать новости добавленные в избранное. 
Чтобы добавить новость в избранное с главного экрана, нужно смахнуть ее слева направо, чтобы удалить новость из избранного с главного экрана - справа налево.
Добавить или удалить новости из избранного на дополнительном экране можно по кнопке снизу справа.

## Реализованно

- Основной экран: отображение данных, полученных из удаленного [API](https://newsapi.org/)
- Дополнительный экран: экран, доступный из основного, с подробностями о статье
- Дополнительный экран (избранное): экран, доступный из основного с избранными новостями
- Поддержана светлая и темная темы
- Правила flutter_lints включены и нет ошибок
- Код отформатирован
- Разделение проекта на слои и декомпозиция логики
- DI с использованием одного из общепринятых подходов
- Использование одного из общепринятых подходов для реализации управления состоянием UI (Riverpod)
- json_serializable модель для статьи
- Интернационализация
- TabBar
- Написаны тесты
- Прикреплен APK-файл
- Добавлена анимация открытия экрана детальной информации (Hero: текст и звездочка)
- Реализованы анимации смены иконок смены темы и языка
- Использованы Sliver'ы
- Реализована поисковая строка, которая пропадает при скролле вниз и появляется при скролле вниз (поиск реализован по слову в заголовке)
- Реализованы фильтры новостей (по дате и по наличию изображения)
- Реализована проверка кода и сборка APK на CI

## Скриншоты

### Главный экран, топ новости, светлая тема, язык английский, добавление и удаление из избранного
![Главный экран, топ новости, светлая тема, язык английский](screenshots/1.jpg) ![Добавление в избранное](screenshots/5.jpg) ![Удаление из избранного](screenshots/6.jpg)
### Дополнительный экран, топ новости, светлая тема, язык английский добавление и удаление из избранного
![Дополнительный экран, топ новости, светлая тема, язык английский](screenshots/2.jpg) ![Добавление в избранное](screenshots/7.jpg) ![Удаление из избранного](screenshots/8.jpg)
### Главный экран, котики, темная тема, язык руский
![Главный экран, котики, темная тема, язык руский](screenshots/3.jpg) ![Добавление в избранное](screenshots/9.jpg) ![Удаление из избранного](screenshots/10.jpg)
### Дополнительный экран, котики, темная тема, язык руский
![Дополнительный экран, котики, темная тема, язык руский](screenshots/4.jpg) ![Добавление в избранное](screenshots/11.jpg) ![Удаление из избранного](screenshots/12.jpg)
### Экран избранное
![Избранное, светлая тема, язык английский](screenshots/13.jpg) ![Избранное, темная тема, язык русский](screenshots/14.jpg)
### Главный экран с поисковой строкой
![Главный экран с поисковой строкой](screenshots/15.jpg) ![Поисковая строка убирается при скролле](screenshots/16.jpg) ![Поиск по слову в заголовке](screenshots/17.jpg)
### Всплывающее окно с фильтрами
![Всплывающее окно с фильтрами](screenshots/18.jpg)



## Начало работы

Чтобы запустить приложение:

```bash
  git clone https://gitlab.com/deeelis/homework-flutter-2024.git
  cd homework-flutter-2024
  Flutter flutter pub get
  flutter run
```

Либо можете установить с помощью APK-файла

## Статус проекта   

В разработке..
