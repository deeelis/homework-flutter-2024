import 'package:first_homework/core/app/my_app.dart';
import 'package:first_homework/ui/providers/localization_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Check changing locale', (tester) async {
    await tester.pumpWidget(
      const ProviderScope(
        child: MyApp(),
      ),
    );


    final element = tester.element(find.byKey(const ValueKey<String>("EN")));
    final icon = find.byKey(const ValueKey<String>("EN"));
    final container = ProviderScope.containerOf(element);

    await tester.tap(icon);
    await tester.pump();
    expect(
      container.read(localizationProvider),
      'ru',
    );
    await tester.pumpWidget(const Placeholder());
    addTearDown(container.dispose);
  });

  testWidgets(
    'Check if tap on the favorites button, the favorites screen opens',
    (tester) async {
      await tester.pumpWidget(
        const ProviderScope(
          child: MyApp(),
        ),
      );

      final button = find.byKey(const Key("fav"));
      await tester.tap(button);
      await tester.pump(const Duration(milliseconds: 400));

      final favScreen = find.byKey(const Key("favPage"));
      await tester.pump(const Duration(milliseconds: 400));

      expect(favScreen, findsOneWidget);
    },
  );
}
