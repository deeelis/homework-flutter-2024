import 'package:first_homework/data/dao/articles_dao.dart';
import 'package:first_homework/data/dao/articles_runtime_dao.dart';
import 'package:first_homework/data/dao/favourites_dao.dart';
import 'package:first_homework/data/dao/favourites_runtime_dao.dart';
import 'package:first_homework/data/dto/article_dto.dart';
import 'package:first_homework/data/dto/articles_api.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

class MockArticleDao extends Mock implements ArticlesDao {}

class MockFavouritesDao extends Mock implements FavouritesDao {}

void main() {
  late final ArticlesDao articlesDao;
  late final FavouritesRuntimeDao favouritesDao;

  setUpAll(() {
    articlesDao = ArticlesRuntimeDao();
    sqfliteFfiInit();
    databaseFactory = databaseFactoryFfi;
    favouritesDao = FavouritesRuntimeDao();
  });

  test('Check getting data by API', () async {
    ArticlesApi? articles = await articlesDao.getData(
      0,
      'https://newsapi.org/v2/top-headlines?language=en&apiKey=feaa635275eb4abda0fa41e837a172ba',
      '',
    );

    expect(articles?.status, 'ok');
    expect(articles?.articles.length, greaterThan(0));
  });

  test('Check database adding and deletion', () async {
    const article1 = ArticleDto(
      'uuid1',
      'author1',
      'title1',
      'description1',
      'url1',
      'urlToImage1',
      'publishedAt1',
    );
    const article2 = ArticleDto(
      'uuid2',
      'author2',
      'title2',
      'description2',
      'url2',
      'urlToImage2',
      'publishedAt2',
    );
    favouritesDao
      ..addFavorite(article1)
      ..addFavorite(article2);
    List<ArticleDto> articles = await favouritesDao.getAll();
    expect(articles.length, 2);
    expect(articles[0], article1);
    favouritesDao.deleteFavorite(article1);
    articles = await favouritesDao.getAll();
    expect(articles.length, 1);
    expect(articles[0], article2);
  });
}
